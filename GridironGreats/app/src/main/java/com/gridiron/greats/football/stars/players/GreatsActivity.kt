package com.gridiron.greats.football.stars.players

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gridiron.greats.football.stars.players.databinding.ActivityGreatsBinding

class GreatsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityGreatsBinding

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGreatsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        binding.ivBack.setOnClickListener {
            onBackPressed()
        }

        binding.greatsRecycle.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)


        val greatsData = ArrayList<Greats>()
        greatsData.add(
            Greats(
                "Tom \nBrady",
                R.drawable.greats_1,
                "When discussing the greatest quarterbacks in NFL history, Tom Brady's name inevitably finds itself at the top of the list. Known for his uncanny ability to thrive under pressure and dissect defenses with surgical precision, he has demonstrated time and again that no deficit is insurmountable when he's leading the charge. As the centerpiece of the New England Patriots' two-decade-long dynasty, Brady's legacy is marked by numerous iconic playoff performances and Super Bowl victories."
            )
        )

        greatsData.add(
            Greats(
                "Barry \nSanders",
                R.drawable.greats_2,
                "Barry Sanders was a human highlight reel who left defenders grasping at air with his strong moves, agility, and breakaway speed. His quiet, graceful demeanor off the field belied his relentless determination and ability to turn broken plays into touchdown runs. While his career was relatively short-lived, his impact on the sport is timeless.\n"
            )
        )

        greatsData.add(
            Greats(
                "Jerry \nRice",
                R.drawable.greats_3,
                "There's little argument that Jerry Rice is among the greatest wide receivers of all time. With an unrivaled work ethic and meticulous attention to detail, he spent a 20-year career racking up records and delivering jaw-dropping performances. Known for his skilled route-running and vice-like grip, Rice was an artist on the field, transforming the aerial attack and inspiring countless future players.\n"
            )
        )

        greatsData.add(
            Greats(
                "Walter \nPayton",
                R.drawable.greats_4,
                "Affectionately known as \"Sweetness,\" Walter Payton was the epitome of grace, power, and determination throughout his career. With a never-quit attitude, he set the gold standard for what it meant to be a running back and secured his place among the NFL's elite. Even in the face of adversity, his unwavering dedication to his craft and commitment to punishing defenders made Payton one of the most respected players in league history. He passed at age 45 from complications of primary sclerosing cholangitis and bile duct cancer.\n"
            )
        )

        greatsData.add(
            Greats(
                "Jim \nBrown",
                R.drawable.greats_5,
                "A titan of the sport, Jim Brown was an unstoppable force who set the standard for future generations of running backs. With his unique blend of size, speed, and raw power, he terrorized defenses and left countless would-be tacklers in his wake. Even after retiring from the game at the peak of his powers, his impact on the sport remains, and his legend looms large over the gridiron to this day.\n"
            )
        )

        greatsData.add(
            Greats(
                "Reggie \nWhite",
                R.drawable.greats_6,
                "Known as the \"Minister of Defense,\" Reggie White was a force of nature who struck fear into the hearts of opposing quarterbacks with his combination of speed and power. A dominant pass rusher capable of changing the course of games singlehandedly, his tenacity and fearless style of play not only made him one of the greatest defensive linemen of all time, but also left an indelible mark on the sport after his premature passing from complications of sarcoidosis.\n"
            )
        )

        greatsData.add(
            Greats(
                "Randy \nMoss",
                R.drawable.greats_7,
                "The embodiment of the term \"freak athlete,\" Randy Moss could stretch the field like no other before or since, making extraordinary catches look routine and leaving defenders in the dust. With his formidable combination of size, speed, and sheer athleticism, he became one of the most iconic wide receivers in NFL history. Changing the way offenses approached the passing game, he left a lasting impact on the sport as a whole."
            )
        )

        greatsData.add(
            Greats(
                "Dick \nButkus",
                R.drawable.greats_8,
                "Known for his ferocious tackles and no-nonsense demeanor, Dick Butkus was the quintessential middle linebacker who set the bar for all who followed. With an almost supernatural ability to disrupt offenses and unnerve opposing ball carriers, he was a player who left everything on the field. His passionate playing style has since become synonymous with his position, setting the standard for how middle linebackers should be judged in both character and performance."
            )
        )

        greatsData.add(
            Greats(
                "Deion \nSanders",
                R.drawable.greats_9,
                "“Prime Time” Deion Sanders was the epitome of a shutdown cornerback, demoralizing opposing quarterbacks with his impressive instincts and athleticism. But he didn't just dominate on defense; his speed and swagger were also on full display when he returned kicks for touchdowns or took the field as a wide receiver. Sanders' dynamic skills and infectious charisma cemented his status as an NFL icon."
            )
        )

        greatsData.add(
            Greats(
                "Peyton \nManning",
                R.drawable.greats_10,
                "When it comes to masterful quarterback play, few can hold a candle to Peyton Manning's impressive resume. With an uncanny ability to read defenses and make adjustments at the line of scrimmage, his cerebral approach propelled him to many heights throughout his career. Manning's pinpoint accuracy and unwavering dedication left a lasting impact on football, inspiring future signal-callers to study the game from a strategic standpoint."
            )
        )

        greatsData.add(
            Greats(
                "Calvin \nJohnson",
                R.drawable.greats_11,
                "Nicknamed \"Megatron\" for his otherworldly combination of size, speed, and athleticism, Calvin Johnson was known for intimidating opposing defensive backs throughout his career. Using both physicality and agility, his ability to dominate games through sheer force of will placed him among the most feared and respected receivers in NFL history. He chose to take an early retirement, but his impact on the game remains undeniable."
            )
        )

        greatsData.add(
            Greats(
                "Bo \nJackson",
                R.drawable.greats_12,
                "Although his career was cut short due to a hip injury, Bo Jackson's two-sport exploits remain the stuff of legend. As both an NFL running back and MLB outfielder, he showed both athleticism and explosiveness that captivated fans worldwide. His time in the spotlight was brief, but Jackson's impact endures on the sports of both football and baseball."
            )
        )

        greatsData.add(
            Greats(
                "Ray \nLewis",
                R.drawable.greats_13,
                "A leader in every sense of the word, Ray Lewis was a force to be reckoned with on the football field. His intensity and passion for the game translated into powerful hits and unmatched instincts that made him one of the most feared linebackers in the NFL. Lewis, who retired in 2012, was also known for his ability to motivate his teammates and inspire greatness in those around him.\n"
            )
        )

        greatsData.add(
            Greats(
                "Larry \nFitzgerald",
                R.drawable.greats_14,
                "Larry Fitzgerald is well known for his humility and his professionalism - and for being one football's most talented wide receivers. With incredible hands, elite route-running skills, and an unparalleled work ethic, he consistently amazed fans with his acrobatic catches and ability to make contested grabs in traffic. As a role model both on and off the field, his legacy as a consummate professional in the league will endure long past his 2022 retirement.\n"
            )
        )

        greatsData.add(
            Greats(
                "Charles \nWoodson",
                R.drawable.greats_15,
                "A versatile and dominant defensive back, Charles Woodson's combination of instincts, athleticism, and playmaking ability earned him a reputation as a game-changer throughout his lengthy career. As a shutdown cornerback and later a ball-hawking safety, his ability to adapt to different positions and excel at each demonstrated both his skill and his commitment to his craft. One of the most respected and accomplished defenders in league history, Woodson's legacy will endure for generations."
            )
        )

        greatsData.add(
            Greats(
                "Earl \nCampbell",
                R.drawable.greats_16,
                "A human wrecking ball, Earl Campbell was a fearless, powerful running back who left countless defenders in his wake. With his seemingly unstoppable momentum, Campbell made an indelible impression on football during his rather short career from the mid-1970s to mid-'80s. His impact on the game is still present today, as his bruising style and sheer determination continue to inspire future generations of runners."
            )
        )

        greatsData.add(
            Greats(
                "Ed \nReed",
                R.drawable.greats_17,
                "With an impressive ability to read quarterbacks and sniff out passing plays, Ed Reed was a feared ballhawk and game-changing presence in the defensive secondary. Known for his quick instincts, keen mind, and talented playmaking, Reed intimidated offenses throughout his career and helped redefine the role of the modern safety. He was also a leader and mentor to his teammates.\n"
            )
        )

        greatsData.add(
            Greats(
                "JJ \nWatt",
                R.drawable.greats_18,
                "A dominant force on the defensive line, JJ Watt has menaced quarterbacks and crushed offensive game plans throughout his illustrious career. With a relentless work ethic and passion for the game, his ability to singlehandedly disrupt opposing offenses has made him a perennial All-Pro and fan favorite. As an inspirational leader both on and off the field, Watt's dedication to excellence has left an indelible mark on the NFL.\n"
            )
        )

        greatsData.add(
            Greats(
                "Joe \nMontana",
                R.drawable.greats_19,
                "Regarded by many as the ultimate winner, Joe Montana's poise and precision under pressure set the standard for championship-caliber quarterback play. With an enviable ability to step up in clutch situations, his cool demeanor and unshakeable confidence helped him guide the 49ers to numerous Super Bowl titles. As a master of the two-minute drill and late-game heroics, Montana's legacy remains an inspiration to quarterbacks around the world.\n"
            )
        )

        greatsData.add(
            Greats(
                "John \nElway",
                R.drawable.greats_20,
                "Clutch under pressure and able to make jaw-dropping plays when it mattered most, John Elway etched his name in NFL immortality with his signature come-from-behind victories and late-game heroics. A dual-threat quarterback with both a rocket arm and impressive mobility, his will to win propelled him to two Super Bowl titles and countless memorable moments throughout his storied career. Elway's legendary performances and impact on the game continue to inspire future generations of signal-callers."
            )
        )

        val adapter = GreatsAdapter(greatsData)
        binding.greatsRecycle.adapter = adapter

        binding.ivNext.setOnClickListener {
            binding.greatsRecycle.postDelayed(Runnable {
                binding.greatsRecycle.smoothScrollToPosition(adapter.currentPositionOfItem + 1)
            }, 500)
            adapter.notifyDataSetChanged()
        }

    }
}