package com.gridiron.greats.football.stars.players

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class GreatsAdapter(private val rules: ArrayList<Greats>) :
    RecyclerView.Adapter<GreatsAdapter.ViewHolder>() {

    var currentPositionOfItem = 0

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val greatsImage: ImageView = itemView.findViewById(R.id.image)
        val greatsTitle: TextView = itemView.findViewById(R.id.title)
        val greatsDescription: TextView = itemView.findViewById(R.id.desc)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.greats_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val rulesList = rules[position]
        rulesList.image?.let { holder.greatsImage.setImageResource(it) }
        holder.greatsTitle.text = rulesList.title
        holder.greatsDescription.text = rulesList.description
        currentPositionOfItem = holder.adapterPosition
    }

    override fun getItemCount(): Int {
        return rules.size
    }
}